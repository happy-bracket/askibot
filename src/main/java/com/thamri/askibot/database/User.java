package com.thamri.askibot.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "users")
public class User {

    @DatabaseField(id = true)
    private int userId;
    @DatabaseField
    private String brush;
    @DatabaseField
    private String background;
    @DatabaseField
    private int depth;
    @DatabaseField
    private byte genType;
    @DatabaseField
    private byte joined;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getBrush() {
        return brush;
    }

    public void setBrush(String brush) {
        this.brush = brush;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    @Override
    public String toString() {
        return "{ userId: " + userId + ", brush: " + brush + ", background: " + background + ", depth: " + depth + " }";
    }

    public byte getGenType() {
        return genType;
    }

    public void setGenType(byte genType) {
        this.genType = genType;
    }

    public byte getJoined() {
        return joined;
    }

    public void setJoined(byte joined) {
        this.joined = joined;
    }
}
