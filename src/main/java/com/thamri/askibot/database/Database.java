package com.thamri.askibot.database;

public interface Database {

    User getUser(int id);

    void setUser(User user);

    void createUser(int userId);

}
