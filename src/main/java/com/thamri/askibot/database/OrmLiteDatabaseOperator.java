package com.thamri.askibot.database;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.thamri.askibot.AppConfig;
import com.thamri.askibot.utils.ConfigLoader;

import javax.inject.Inject;
import java.sql.SQLException;

public class OrmLiteDatabaseOperator implements Database {

    private Dao<User, Integer> userDao = null;
    private User stub;

    @Inject
    public OrmLiteDatabaseOperator(ConfigLoader configLoader) {
        AppConfig dbConfig = configLoader.load("database.json");
        String dbUrl = dbConfig.getValue("url");
        String user = dbConfig.getValue("user");
        String password = dbConfig.getValue("password");
        ConnectionSource cSrc;
        try {
            cSrc = new JdbcConnectionSource(dbUrl, user, password);
            userDao = DaoManager.createDao(cSrc, User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User getUser(int userId) {
        try {
            return userDao.queryForId(userId);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void setUser(User user) {
        try {
            userDao.createOrUpdate(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createUser(int userId) {
        User newUser = new User();
        newUser.setUserId(userId);
        newUser.setBrush("#ffffff");
        newUser.setBackground("#000000");
        newUser.setDepth(12);
        newUser.setGenType((byte) 0);
        newUser.setJoined((byte) 1);
        setUser(newUser);
    }

}
