package com.thamri.askibot.database;

import com.thamri.askibot.utils.ConfigLoader;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
public class DatabaseModule {

    @Singleton
    @Provides
    public Database provideDatabase(ConfigLoader configLoader) {
        return new OrmLiteDatabaseOperator(configLoader);
    }


}
