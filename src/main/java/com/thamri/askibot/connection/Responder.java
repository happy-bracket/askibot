package com.thamri.askibot.connection;

import com.petersamokhin.bots.sdk.clients.Group;

public interface Responder {

    void response(Group group);

}
