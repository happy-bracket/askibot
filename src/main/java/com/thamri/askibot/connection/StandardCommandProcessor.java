package com.thamri.askibot.connection;

import com.petersamokhin.bots.sdk.clients.Group;
import com.petersamokhin.bots.sdk.objects.Message;
import com.thamri.askibot.Main;
import com.thamri.askibot.database.Database;
import com.thamri.askibot.database.User;

import javax.inject.Inject;



public class StandardCommandProcessor implements CommandProcessor {

    // /помощь
    private static String HELP_COMMAND = "/\u043f\u043e\u043c\u043e\u0449\u044c";
    // /цвет
    private static String COLOR_COMMAND = "/\u0446\u0432\u0435\u0442";
    // /фон
    private static String BACKGROUND_COMMAND = "/\u0444\u043e\u043d";

    // /базовый
    private static String BASIC_COMMAND = "/\u0431\u0430\u0437\u043e\u0432\u044b\u0439";
    // /экспериментальный
    private static String EXPERIMENTAL_COMMAND = "/\u044d\u043a\u0441\u043f\u0435\u0440\u0438\u043c\u0435\u043d\u0442\u0430\u043b\u044c\u043d\u044b\u0439";

    // /сброс
    private static String RESET_COMMAND = "/\u0441\u0431\u0440\u043e\u0441";

    private static String START_COMMAND = "/\u0441\u0442\u0430\u0440\u0442";

    private static String HELP_RESPONSE = "/\u0444\u043e\u043d <\u0446\u0432\u0435\u0442> \u0438 /\u0446\u0432\u0435\u0442 <\u0446\u0432\u0435\u0442> - \u0443\u0441\u0442\u0430\u043d\u0430\u0432\u043b\u0438\u0432\u0430\u0435\u0442 \u0446\u0432\u0435\u0442 \u0444\u043e\u043d\u0430, \u0446\u0432\u0435\u0442 \u0434\u043e\u043b\u0436\u0435\u043d \u0431\u044b\u0442\u044c \u0443\u043a\u0430\u0437\u0430\u043d \u0432 \u0444\u043e\u0440\u043c\u0430\u0442\u0435 HEX \n /\u0441\u0431\u0440\u043e\u0441 - \u0441\u0431\u0440\u0430\u0441\u044b\u0432\u0430\u0435\u0442 \u0432\u0441\u0435 \u043d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \n /\u0431\u0430\u0437\u043e\u0432\u044b\u0439 \u0438 /\u044d\u043a\u0441\u043f\u0435\u0440\u0438\u043c\u0435\u043d\u0442\u0430\u043b\u044c\u043d\u044b\u0439 - \u0443\u0441\u0442\u0430\u043d\u0430\u0432\u043b\u0438\u0432\u0430\u044e\u0442 \u0431\u0430\u0437\u043e\u0432\u044b\u0439 \u0438 \u044d\u043a\u0441\u043f\u0435\u0440\u0438\u043c\u0435\u043d\u0442\u0430\u043b\u044c\u043d\u044b\u0439 \u0440\u0435\u0436\u0438\u043c\u044b \u0441\u043e\u043e\u0442\u0432\u0435\u0442\u0441\u0442\u0432\u0435\u043d\u043d\u043e";
    private static String OK_RESPONSE = "\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0441\u043e\u0445\u0440\u0430\u043d\u0435\u043d\u044b.";
    private static String NOT_OK_RESPONSE = "\u041a\u043e\u043c\u0430\u043d\u0434\u0430 \u043d\u0430\u0431\u0440\u0430\u043d\u0430 \u043d\u0435\u0432\u0435\u0440\u043d\u043e. \u041f\u043e\u0436\u0430\u043b\u0443\u0439\u0441\u0442\u0430, \u043f\u043e\u043f\u0440\u043e\u0431\u0443\u0439\u0442\u0435 \u0435\u0449\u0435 \u0440\u0430\u0437";

    @Inject public Database database;

    @Inject
    public StandardCommandProcessor() {
        Main.component.inject(this);
    }

    @Override
    public void process(Group group) {

        processHelpCommand(group);

        processColorCommand(group);

        processBackgroundCommand(group);

        processResetCommand(group);

        processBaseCommand(group);

        processExpCommand(group);

        processStartCommand(group);

    }

    private void processStartCommand(Group group) {
        group.onCommand(START_COMMAND, command -> database.createUser(command.authorId()));
    }

    private void processBaseCommand(Group group) {
        group.onCommand(BASIC_COMMAND, command -> {
            User user = database.getUser(command.authorId());
            user.setGenType((byte) 0);
            database.setUser(user);
            new Message().to(command.authorId()).from(group).text(OK_RESPONSE).send();
        });
    }

    private void processExpCommand(Group group) {
        group.onCommand(EXPERIMENTAL_COMMAND, command -> {
            User user = database.getUser(command.authorId());
            user.setGenType((byte) 1);
            database.setUser(user);
            new Message().to(command.authorId()).from(group).text(OK_RESPONSE).send();
        });
    }

    private void processResetCommand(Group group) {
        group.onCommand(RESET_COMMAND, command -> {
            User user = database.getUser(command.authorId());
            user.setGenType((byte) 0);
            user.setBackground("#000000");
            user.setBrush("#ffffff");
            user.setDepth(12);
            database.setUser(user);
            new Message().text(OK_RESPONSE).from(group).to(command.authorId()).send();
        });
    }


    private void processBackgroundCommand(Group group) {
        group.onCommand(BACKGROUND_COMMAND, message -> {
            User user = database.getUser(message.authorId());
            Message response = new Message();
            response.to(message.authorId()).from(group);
            String color = message.getText().split(" ")[1];
            if (color.matches("#[0-f]{6}")) {
                user.setBackground(color);
                database.setUser(user);
                response.text(OK_RESPONSE);
            } else {
                response.text(NOT_OK_RESPONSE);
            }
            response.send();
        });
    }

    private void processHelpCommand(Group group) {
        group.onCommand(HELP_COMMAND, message -> new Message().to(message.authorId()).from(group).text(HELP_RESPONSE).send());
    }

    private void processColorCommand(Group group) {
        group.onCommand(COLOR_COMMAND, message -> {
            User user = database.getUser(message.authorId());
            Message response = new Message();
            response.to(message.authorId()).from(group);
            String color = message.getText().split(" ")[1];
            if (color.matches("#[0-f]{6}")) {
                user.setBrush(color);
                database.setUser(user);
                response.text(OK_RESPONSE);
            } else {
                response.text(NOT_OK_RESPONSE);
            }
            response.send();
        });
    }

}
