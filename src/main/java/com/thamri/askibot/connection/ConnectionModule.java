package com.thamri.askibot.connection;

import dagger.Module;
import dagger.Provides;

@Module
public class ConnectionModule {

    @Provides
    public CommandProcessor provideCommandProcessor() {
        return new StandardCommandProcessor();
    }

    @Provides
    public Responder provideResponder() {
        return new SimpleResponder();
    }
}
