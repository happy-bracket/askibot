package com.thamri.askibot.connection;

import com.petersamokhin.bots.sdk.clients.Group;
import com.petersamokhin.bots.sdk.objects.Message;

import javax.inject.Inject;
import java.util.Random;

public class SimpleResponder implements Responder {

    private String[] responses = {
            "\u041f\u0440\u0438\u0432\u0435\u0442! \u041e\u0442\u043f\u0440\u0430\u0432\u044c \u043c\u043d\u0435 \u043a\u0430\u0440\u0442\u0438\u043d\u043a\u0443 \u0438 \u043f\u043e\u043b\u0443\u0447\u0438 \u0441\u0432\u043e\u0439 ASCII-\u0430\u0440\u0442! \u0415\u0441\u043b\u0438 \u0445\u043e\u0447\u0435\u0448\u044c \u043f\u043e\u0441\u043c\u043e\u0442\u0440\u0435\u0442\u044c \u043a\u043e\u043c\u0430\u043d\u0434\u044b, \u043d\u0430\u0431\u0435\u0440\u0438 /\u043f\u043e\u043c\u043e\u0449\u044c ;)",
    };

    private Random random;

    @Inject
    public SimpleResponder() {
        random = new Random();
    }

    @Override
    public void response(Group group) {
        group.onSimpleTextMessage(message -> {
            System.out.println(message.getText());
            new Message().to(message.authorId()).from(group).text(getResponse()).send();
        });
    }

    private String getResponse() {
        return responses[random.nextInt(responses.length)];
    }

}
