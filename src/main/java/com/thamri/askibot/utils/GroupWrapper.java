package com.thamri.askibot.utils;

import com.petersamokhin.bots.sdk.clients.Group;
import com.thamri.askibot.AppConfig;

import javax.inject.Singleton;

@Singleton
public class GroupWrapper {

    private Group group;

    public GroupWrapper(AppConfig config) {
        String status = config.getValue("dev_status");
        int id = Integer.valueOf(config.getValue(status + ".id"));
        String key = config.getValue(status + ".access_key");
        group = new Group(id, key);
    }

    public Group group() {
        return group;
    }

}
