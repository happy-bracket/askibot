package com.thamri.askibot.files;

import com.thamri.askibot.AppConfig;
import com.thamri.askibot.utils.ConfigLoader;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Logger;

public class ImageSaver implements FilesSaver {

    private String format;
    private String path;
    private int cur;
    private Thread clearing;

    public ImageSaver(ConfigLoader configLoader) {
        cur = 0;
        AppConfig config = configLoader.load("config.json");
        path = config.getValue(config.getValue("dev_status") + ".image_directory");
        clearing = new Thread(() -> {
            while (true)
                try {
                    Thread.sleep(60000);
                    Path imgDir = Paths.get(path);
                    System.out.println("CLEAAARING");
                    Files.list(imgDir).filter(path -> {
                        try {
                            long crms = Files.readAttributes(path, BasicFileAttributes.class).creationTime().toMillis();
                            long smms = System.currentTimeMillis();
                            return (smms - crms ) > 60000;
                        } catch (IOException e) {
                            e.printStackTrace();
                            return false;
                        }
                    }).forEach(path -> {
                        try {
                            Files.delete(path);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                    synchronized (this) {
                        cur = 0;
                    }
                } catch (InterruptedException | IOException e) {
                    e.printStackTrace();
                }
        });
        clearing.start();
    }

    @Override
    public String save(Object toSave) {
        String saved = "";
        try {
            String da = path + cur + ".png";
            ImageIO.write((BufferedImage) toSave, "png", new java.io.File(da));
            saved = da;
            cur++;
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("saved: " + saved);
        return saved;
    }

}
