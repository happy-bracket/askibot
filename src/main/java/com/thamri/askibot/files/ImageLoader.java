package com.thamri.askibot.files;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

public class ImageLoader implements FilesLoader {

    @Inject
    public ImageLoader() {
    }

    @Override
    public BufferedImage load(String url) {
        System.out.println("ty huy");
        BufferedImage loaded = null;
        try {
            System.out.println(url);
            loaded = ImageIO.read(new URL(url));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return loaded;
    }

}
