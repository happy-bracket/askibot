package com.thamri.askibot.files;

public interface FilesLoader {

    Object load(String url);

}
