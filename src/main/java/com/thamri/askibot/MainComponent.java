package com.thamri.askibot;

import com.thamri.askibot.artgenerator.ArtModule;
import com.thamri.askibot.artgenerator.Aski;
import com.thamri.askibot.connection.ConnectionModule;
import com.thamri.askibot.connection.StandardCommandProcessor;
import com.thamri.askibot.database.DatabaseModule;
import com.thamri.askibot.files.FilesModule;
import com.thamri.askibot.utils.UtilsModule;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = {ArtModule.class, UtilsModule.class, ConnectionModule.class, FilesModule.class, DatabaseModule.class})
public interface MainComponent {

    Connector connect();

    void inject(Aski artGenerator);

    void inject(StandardCommandProcessor commandProcessor);

}
