package com.thamri.askibot.artgenerator;

import java.awt.*;
import java.awt.image.BufferedImage;

public class StandardImageRenderer implements ImageRenderer {

    @Override
    public BufferedImage render(char[][] art, String background, String brush) {
        BufferedImage result = new BufferedImage(art.length * 12, art[0].length * 12, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = result.createGraphics();
        g.setColor(Color.decode(background));
        g.fillRect(0, 0, result.getWidth(), result.getHeight());
        g.setColor(Color.decode(brush));
        for (int i = 0; i < art.length; i++) {
            for (int j = 0; j < art[i].length; j++) {
                g.drawString(String.valueOf(art[i][j]), i * 12 + 6, j * 12 + 6);
            }
        }
        return result;
    }

}
