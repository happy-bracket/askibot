package com.thamri.askibot.artgenerator;

import java.awt.image.BufferedImage;

public interface ImageRenderer {

    BufferedImage render(char[][] art, String background, String brush);
}
