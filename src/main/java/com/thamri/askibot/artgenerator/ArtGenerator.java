package com.thamri.askibot.artgenerator;

import com.petersamokhin.bots.sdk.objects.Message;

public interface ArtGenerator {

    Art generate(Message source);

}
