package com.thamri.askibot.artgenerator;

import java.io.File;

public interface DocRenderer {

    File render(char[][] rawArt);

}
