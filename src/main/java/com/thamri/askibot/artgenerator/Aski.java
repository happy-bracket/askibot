package com.thamri.askibot.artgenerator;

import com.petersamokhin.bots.sdk.objects.Message;
import com.thamri.askibot.Main;
import com.thamri.askibot.database.Database;
import com.thamri.askibot.database.User;
import com.thamri.askibot.files.FilesLoader;
import com.thamri.askibot.files.FilesSaver;

import javax.inject.Inject;
import javax.inject.Named;
import java.awt.*;
import java.awt.image.BufferedImage;

public class Aski implements ArtGenerator {

    @Inject public ImageRenderer imageRenderer;
    @Inject public FilesSaver filesSaver;
    @Inject public FilesLoader filesLoader;
    @Inject @Named("BasicGenerator") public AsciiMapGenerator generator;
    @Inject @Named("ExperimentalGenerator") public AsciiMapGenerator experimentalGenerator;
    @Inject public Database database;

    @Inject
    public Aski() {
        Main.component.inject(this);
    }

    @Override
    public Art generate(Message source) {
        BufferedImage srcImage = (BufferedImage) filesLoader.load(source.getBiggestPhotoUrl(source.getPhotos()));

        User user = database.getUser(source.authorId());

        System.out.println(user);

        byte genType = 0;
        String brush = user.getBrush();
        String background = user.getBackground();
        boolean inverse = Color.decode(brush).getRGB() < Color.decode(background).getRGB();

        char[][] rawArt;

        switch (genType) {
            case 0:
                rawArt  = generator.generate(srcImage, 12, inverse);
                break;
            case 1:
                rawArt = experimentalGenerator.generate(srcImage, user.getDepth(), inverse);
                break;
            default:
                rawArt = new char[0][];
        }

        BufferedImage output = imageRenderer.render(rawArt, background, brush);

        return new Art(filesSaver.save(output), null);
    }

}
