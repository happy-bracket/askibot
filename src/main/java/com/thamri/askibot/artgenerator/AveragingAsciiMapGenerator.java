package com.thamri.askibot.artgenerator;

import java.awt.image.BufferedImage;
import java.util.HashMap;

public class AveragingAsciiMapGenerator implements AsciiMapGenerator {

    private char[] availableChars = {
            ' ',
            '.',
            ':',
            ',',
            '-',
            ';',
            '"',
            '~',
            '_',
            '!',
            '*',
            '+',
            '0',
            '=',
            '$',
            '#',
            '%',
            '@',
            'M'
    };

    @Override
    public char[][] generate(BufferedImage image, int depth, boolean inverse) {

        int resolution = (image.getWidth() + 110) / 111;

        int artWidth = image.getWidth() / resolution;
        int artHeight = image.getHeight() / resolution;
        char[][] map = new char[artWidth][artHeight];

        HashMap<Integer, Character> conformityTable = new HashMap<>(depth);

        int min = 0x00ffffff;
        int max = 0x00000000;
        for(int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {
                int c = image.getRGB(i, j) & 0x00ffffff;
                if (min > c) min = c;
                if (max < c) max = c;
            }
        }

        int step = (max - min) / depth;
        int depthStep = availableChars.length / depth;
        int d = 0;
        if (!inverse) {
            for (int i = min; i < max; i += step) {
                conformityTable.put(i, availableChars[d]);
                d += depthStep;
            }
        } else {
            for (int i = max; i > min; i -= step) {
                conformityTable.put(i, availableChars[d]);
                d += depthStep;
            }
        }

        for (int i = 0; i < artWidth; i++) {
            for (int j = 0; j < artHeight; j++) {
                map[i][j] = conform(curtail(image, i * image.getWidth() / artWidth, j * image.getHeight() / artHeight, image.getWidth() / artWidth, image.getHeight() / artHeight), conformityTable);
            }
        }

        return map;
    }

    private char conform(int pixelRGB, HashMap<Integer, Character> conformityTable) {
        int min = 0x00ffffff;
        int appColor = 0x00ffffff;
        for (int i: conformityTable.keySet()) {
            int c = Math.abs(pixelRGB - i);
            if (min > c) {
                min = c;
                appColor = i;
            }
        }
        return conformityTable.get(appColor);
    }

    private int curtail(BufferedImage toCutOutFrom, int x, int y, int width, int height) {
        long totalValue = 0;
        int totalCount = 0;
        for (int i = x; i < ((x + width) > toCutOutFrom.getWidth() ? toCutOutFrom.getWidth() : (x + width)); i++)
            for (int j = y; j < ((y + height) > toCutOutFrom.getHeight() ? toCutOutFrom.getHeight() : (y + height)); j++) {
                totalCount++;
                int color = toCutOutFrom.getRGB(i, j);
                color &= 0x00ffffff;
                totalValue += color;
            }
        return (int) totalValue / totalCount;
    }

}
